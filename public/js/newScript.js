let postData = [];
let likeData = [];

function getposts(){
    return fetch('http://localhost:3000/posts',{method:'GET'})
    .then((res) =>{
        if(res.ok){
          return res.json()
        }
        else{
          return Promise.reject(res.status)
        }
    })
    .then((x) => {
        postData = x
        displayPosts(postData)
        return x
    })
    .catch(err=>{
        console.log(err);
    })
}


function getlikedPosts(){
   return fetch('http://localhost:3000/likes',{method:'GET'})
   .then(res =>{
    if(res.ok){
        return res.json();
    }
    else{
        return Promise.reject(res.status);
    }
   })
   .then(res =>{
     likeData = res;
     displaylikedPosts(likeData);
     return res;
   })
}



let displayPosts = (x) => {
    let mainId = document.getElementById("posts");
    let htmlString = "";
    x.forEach(element => {
        htmlString += `<div style="margin:20px 0 20px 0;">
                      <li><b>${element.title}</b></li>
                      <li>${element.description}</li>  
                      <li><button class='btn btn-primary' onclick='addLikedPosts(${element.id})'>Add To Liked Posts</button></li>
                      </div>
                      `
    })
    mainId.innerHTML = htmlString;
}




let displaylikedPosts = (x) => {
    let mainId = document.getElementById("LikedPosts");
    let htmlString = "";
    x.forEach(element => {
        htmlString +=`<div style="margin:20px 0 20px 0;">
                      <li><i><b>${element.title}</b></i></li>  
                      <li>${element.description}</li>
                      <li><button class="btn btn-danger" onclick="deletePost(${element.id})">Delete post</button></li>  
                     </div>
                      `
    });

    mainId.innerHTML = htmlString

}




function deletePost(id){
    return fetch(`http://localhost:3000/likes/${id}`,{method:'DELETE'})
    .then((res)=>{
        if(res.ok){
            return res.json();
        }
    })
    .then((x)=>{
        likeData.splice(id);
        displaylikedPosts(likeData);
        return likeData;
    })
}



function addLikedPosts(id){
    let post = postData.find(x=>{
        if(x.id == id){
            return x;
        }
    })



 let likedPost = likeData.find(y=>{
        if(y.id == post.id){
            return y;
        }    
    })
    if(likedPost){
        alert("post is already liked");
        return Promise.reject(new Error('post is already liked'));
    }
    else{
        return fetch('http://localhost:3000/likes',{
            method: 'POST',
            headers: {
               'content-type': 'application/json'
            },
            body:JSON.stringify(post)
        })
        .then((res) => {
            if(res.ok){
                return res.json();
            }
        })
        .then(x=>{
            likeData.push(x);
            displaylikedPosts(likeData);
            return likeData;
        })
    }
}